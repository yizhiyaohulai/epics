#pragma once

#include <iostream>
#include <vector>
#include <map>
#include <linux/can.h>
#include "internal_thread.h"

#define DEVICE_NAME "can0"
#define PORT_MAX_NUM 12
#define BOARD_MAX_NUM 48
#define CAN_QUERY_TIME  50000
#define CONCENTRATOR_TICKS_TIME  30000000/CAN_QUERY_TIME

typedef enum {
	CAN_PORT_OUT_NON_EMPTY,
	CAN_PORT_IN_MALFUNCTION,
	CAN_PORT_IN_INIT_TEST,
	CAN_PORT_IN_TEST,
	CAN_PORT_OUT_LEDON,    //4
	CAN_PORT_OUT_ENCODE00,
	CAN_PORT_OUT_ENCODE01,
	CAN_PORT_OUT_ENCODE02,
	CAN_PORT_OUT_ENCODE03,
	CAN_PORT_OUT_ENCODE04,
	CAN_PORT_OUT_ENCODE05,
	CAN_PORT_OUT_ENCODE06,
	CAN_PORT_OUT_ENCODE07,
	CAN_PORT_OUT_ENCODE08,
	CAN_PORT_OUT_ENCODE09,
	CAN_PORT_OUT_ENCODE10,
	CAN_PORT_OUT_CANID0,		//16
	CAN_PORT_NORMAL,
	CAN_CONCENTRATOR_CAN_ID,	//18
	CAN_BOARD_RESET,
	CAN_IDLE,
}CAN_COMMAND;

class DrvAsynConcentrator : public InternalThread {
public:
	can_frame *pframe;
	can_frame *p_t_frame;
	pthread_t ntid;
	explicit DrvAsynConcentrator();
	int readOctet();
	void writeOctet();
	virtual void InternalThreadEntry();
	std::string cluster_id_string;
	size_t  cluster_id;
	size_t  concentrator_can_id;
	std::map<int, int> P_DBoard_Port_Fault;
	std::map<int, int> P_DBoard_Port_InitTest;
	std::map<int, int> P_DBoard_Port_Test;
	std::map<int, int> P_DBoard_Port_Fault_Update;
	std::map<int, int> P_DBoard_Port_InitTest_Update;
	std::map<int, int> P_DBoard_Port_Test_Update;
	std::map<int, int> collect_board_id_map_;

	size_t board_total_num;
	std::map<int, int> P_DBoard_Port_NonEmpty;
	std::map<int, int> P_DBoard_Port_NonEmpty_bk;
	std::map<int, int> P_DBoard_Port_LedOn;
	std::map<int, int> P_DBoard_Port_LedOn_bk;

	std::map<int, std::map<int, std::string>> P_DBoard_Port_ENCODE;
	std::map<int, std::map<int, int>> P_DBoard_Port_ENCODE_Update;
private:
	int  _socket;
	std::map<std::string, std::string> concentrator_string_pv_;
	std::map<std::string, size_t> concentrator_int_pv_;
	std::map<int, std::string> string_canid0_map;
	void ParseCanFrameData();
	void SetFixPvParameter();
	void SystemMemoryInit();
protected:
	std::map<int, std::map<int, std::map<int, std::string> > > P_DBoard_Port_Original_ENCODE;
	std::map<int, std::map<int, std::string>> P_DBoard_Port_Service;
	std::map<int, std::map<int, int>> P_DBoard_Port_Service_Update;
	//std::vector<std::vector<int> > P_DBoard_Port_Service;		//asynParamInt8Array
};












