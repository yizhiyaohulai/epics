#include <cerrno>
#include <csignal>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <fcntl.h>
#include <fstream>
#include <iostream>
#include <linux/can.h>
#include <linux/can/raw.h>
#include <net/if.h>
#include <sstream>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/select.h>
#include <sys/stat.h>
#include <unistd.h>
#include <cadef.h>
#include <ezca.h>

#include "drv_asyn_can.h"

#include <epicsEvent.h>
#include <epicsExport.h>
#include <epicsMutex.h>
#include <epicsString.h>
#include <epicsThread.h>
#include <epicsTime.h>
#include <epicsTimer.h>
#include <epicsTypes.h>
#include <iocsh.h>

DrvAsynConcentrator::DrvAsynConcentrator() {

	sockaddr_can addr;
	ifreq ifr;

	pframe = new can_frame;
	p_t_frame = new can_frame;
	_socket = socket(PF_CAN, SOCK_RAW, CAN_RAW);
	if (_socket < 0) 
		std::cout << "Error while opening socket" << std::endl;

	strcpy(ifr.ifr_name, DEVICE_NAME);
	ioctl(_socket, SIOCGIFINDEX, &ifr);

	addr.can_family = AF_CAN;
	addr.can_ifindex = ifr.ifr_ifindex;

	if (bind(_socket, (struct sockaddr *)&addr, sizeof(addr)) < 0)
		std::cout << "Error in socket bind" << std::endl;
	SystemMemoryInit();
	SetFixPvParameter();
	StartInternalThread();
}

void DrvAsynConcentrator::SystemMemoryInit() {
	for (size_t i = 0; i < BOARD_MAX_NUM; i++) {
		P_DBoard_Port_Fault[i] = 0;
		P_DBoard_Port_InitTest[i] = 0;
		P_DBoard_Port_Test[i] = 0;
		P_DBoard_Port_Fault_Update[i] = 0;
		P_DBoard_Port_InitTest_Update[i] = 0;
		P_DBoard_Port_Test_Update[i] = 0;
		collect_board_id_map_[i] = 0;
		P_DBoard_Port_NonEmpty[i] = 0;
		P_DBoard_Port_NonEmpty_bk[i] = 0;
		P_DBoard_Port_LedOn[i] = 0;
		P_DBoard_Port_LedOn_bk[i] = 0;

		for (size_t j = 0; j < PORT_MAX_NUM; j++) {
			P_DBoard_Port_ENCODE[i][j] = "";
			P_DBoard_Port_ENCODE_Update[i][j] = 0;
			P_DBoard_Port_Original_ENCODE[i][j][0] = "";
			P_DBoard_Port_Original_ENCODE[i][j][1] = "";
			P_DBoard_Port_Original_ENCODE[i][j][2] = "";
			P_DBoard_Port_Original_ENCODE[i][j][3] = "";
			P_DBoard_Port_Original_ENCODE[i][j][4] = "";
			P_DBoard_Port_Original_ENCODE[i][j][5] = "";
			P_DBoard_Port_Original_ENCODE[i][j][6] = "";
		}
	}
}

void DrvAsynConcentrator::SetFixPvParameter() {

	char *path_file = getenv("CONCENTRATORFILE");
	std::ifstream input(path_file, std::ios_base::in);
	std::string line;

	if (!input.is_open())
		fprintf(stderr, "CONCENTRATORFILE not set\n\r");

	while (std::getline(input, line)) {
		// handle comments in config file
		size_t comment;
		if ((comment = line.find('#')) != std::string::npos)
			line.erase(comment); // erase everthing after first '#' from string

		if (0 == line.length()) continue; // string is empty, no need to parse it
		std::string type;
		std::string value;
		std::istringstream parse(line);
		parse >> type;
		parse >> value;
		if (type.find("Cluster") != std::string::npos || \
			type.find("CanId") != std::string::npos)
			concentrator_int_pv_[type] = atoi(value.c_str());
		else if (type.find("Building") != std::string::npos || \
			type.find("FacilityRoom") != std::string::npos || \
			type.find("Cabinet") != std::string::npos || \
			type.find("Name") != std::string::npos)
			concentrator_string_pv_[type] = value;
	}

	cluster_id = concentrator_int_pv_["Cluster:"];
	concentrator_can_id = concentrator_int_pv_["CanId:"];
	char cluster_id_char[8];
	sprintf(cluster_id_char, "%02d", cluster_id);
	cluster_id_string = cluster_id_char;

	std::string cluster_name_building_string = "Cluster" + cluster_id_string + ":Name:Building";
	//ezcaPut((char *)cluster_name_building_string.c_str(), ezcaByte, concentrator_string_pv_["Building:"].size(), \
				(void *)concentrator_string_pv_["Building:"].c_str());
	std::string building_string_cmd = "caput " + cluster_name_building_string + "  " +concentrator_string_pv_["Building:"];
	
	std::string cluster_name_facilityroom_string = "Cluster" + cluster_id_string  + ":Name:FacilityRoom";
	//ezcaPut((char *)cluster_name_building_string.c_str(), ezcaByte, concentrator_string_pv_["FacilityRoom:"].size(), \
			(void *)concentrator_string_pv_["FacilityRoom:"].c_str());
	std::string facilityroom_string_cmd= "caput " + cluster_name_facilityroom_string + "  "  + concentrator_string_pv_["FacilityRoom:"];
	
	std::string cluster_name_string = "Cluster" + cluster_id_string  + ":Name";
	//ezcaPut((char *)cluster_name_building_string.c_str(), ezcaByte, concentrator_string_pv_["Name:"].size(), \
			(void *)concentrator_string_pv_["Name:"].c_str());
	std::string name_cmd = "caput " + cluster_name_string + "  "  + concentrator_string_pv_["Name:"];

	p_t_frame->can_id = concentrator_can_id;
	p_t_frame->can_dlc = 1;
	p_t_frame->data[0] = CAN_BOARD_RESET;
	writeOctet();

	usleep(3000000);
	system(building_string_cmd.c_str());
	system(facilityroom_string_cmd.c_str());
	system(name_cmd.c_str());

	int *rcs, nrcs;
	ezcaEndGroupWithReport(&rcs, &nrcs);
	ezcaFree((void *)rcs);
	//send concentrator_can_id

	p_t_frame->can_id = concentrator_can_id;
	p_t_frame->can_dlc = 5;
	p_t_frame->data[0] = CAN_CONCENTRATOR_CAN_ID;
	p_t_frame->data[1] = concentrator_can_id >> 24;
	p_t_frame->data[2] = (concentrator_can_id >> 16) &0xff;
	p_t_frame->data[3] = (concentrator_can_id >> 8) & 0xff;
	p_t_frame->data[4] = concentrator_can_id & 0xff;
	writeOctet();
}

void DrvAsynConcentrator::ParseCanFrameData() {

	int dispatch_can_id{ 0 };
	int board_id{ 0 };
	CAN_COMMAND cmd = (CAN_COMMAND)pframe->data[0];
	switch (cmd) {
		case CAN_PORT_OUT_NON_EMPTY:                     //in
			dispatch_can_id = pframe->can_id;
			for (auto it : collect_board_id_map_) {
				if (it.second == dispatch_can_id) {
					auto board_id = it.first;
					P_DBoard_Port_NonEmpty[board_id] = (pframe->data[1] << 8) | pframe->data[2];
					printf("PORT_EMPTY: boardid:%d,port:%d\n\r", board_id, P_DBoard_Port_NonEmpty[board_id]);
					break;
				}
			}
			break;
		case CAN_PORT_OUT_LEDON:                        //in
			dispatch_can_id = pframe->can_id;
			for (auto it : collect_board_id_map_) {
				if ((it.second& 0xffff) == dispatch_can_id) {
					auto board_id = it.first;
					P_DBoard_Port_LedOn[board_id] = (pframe->data[1] << 8) | pframe->data[2];
					break;
				}
			}
			break;
		case CAN_PORT_OUT_CANID0:    
			dispatch_can_id = (pframe->data[1] << 24) | (pframe->data[2]<< 16) | (pframe->data[3] << 8) | pframe->data[4];
			board_id = pframe->data[5];
			collect_board_id_map_[board_id] = dispatch_can_id;
			break;
		case CAN_PORT_OUT_ENCODE00:
			dispatch_can_id = pframe->can_id;
			for (auto it : collect_board_id_map_) {
				if (it.second == dispatch_can_id) {
					auto board_id = it.first;
					auto port_id = pframe->data[1];
					P_DBoard_Port_Original_ENCODE[board_id][port_id][0] = (const char *)(pframe->data + 2);
					break;
				}
			}
			break;
		case CAN_PORT_OUT_ENCODE01:
			dispatch_can_id = pframe->can_id;
			for (auto it : collect_board_id_map_) {
				if (it.second == dispatch_can_id) {
					auto board_id = it.first;
					auto port_id = pframe->data[1];
					P_DBoard_Port_Original_ENCODE[board_id][port_id][1] = (const char *)(pframe->data + 2);
					break;
				}
			}
			break;
		case CAN_PORT_OUT_ENCODE02:
			dispatch_can_id = pframe->can_id;
			for (auto it : collect_board_id_map_) {
				if (it.second == dispatch_can_id) {
					auto board_id = it.first;
					auto port_id = pframe->data[1];
					P_DBoard_Port_Original_ENCODE[board_id][port_id][2] = (const char *)(pframe->data + 2);
					break;
				}
			}
			break;
		case CAN_PORT_OUT_ENCODE03:
			dispatch_can_id = pframe->can_id;
			for (auto it : collect_board_id_map_) {
				if (it.second == dispatch_can_id) {
						auto board_id = it.first;
						auto port_id = pframe->data[1];
						P_DBoard_Port_Original_ENCODE[board_id][port_id][3] = (const char *)(pframe->data + 2);
						break;
				}
			}
			break;
		case CAN_PORT_OUT_ENCODE04:
			dispatch_can_id = pframe->can_id;
			for (auto it : collect_board_id_map_) {
				if (it.second == dispatch_can_id) {
					auto board_id = it.first;
					auto port_id = pframe->data[1];
					P_DBoard_Port_Original_ENCODE[board_id][port_id][4] = (const char *)(pframe->data + 2);
					break;
				}
			}
			break;
		case CAN_PORT_OUT_ENCODE05:
			dispatch_can_id = pframe->can_id;
			for (auto it : collect_board_id_map_) {
				if (it.second == dispatch_can_id) {
					auto board_id = it.first;
					auto port_id = pframe->data[1];
					P_DBoard_Port_Original_ENCODE[board_id][port_id][5] = (const char *)(pframe->data + 2);
					break;
				}
			}
			break;
		case CAN_PORT_OUT_ENCODE06:
			dispatch_can_id = pframe->can_id;
			for (auto it : collect_board_id_map_) {
				if (it.second == dispatch_can_id) {
					auto board_id = it.first;
					auto port_id = pframe->data[1];
					P_DBoard_Port_Original_ENCODE[board_id][port_id][6] = (const char *)(pframe->data + 2);
					P_DBoard_Port_ENCODE[board_id][port_id] = P_DBoard_Port_Original_ENCODE[board_id][port_id][0] + \
						P_DBoard_Port_Original_ENCODE[board_id][port_id][1] + \
						P_DBoard_Port_Original_ENCODE[board_id][port_id][2] + \
						P_DBoard_Port_Original_ENCODE[board_id][port_id][3] + \
						P_DBoard_Port_Original_ENCODE[board_id][port_id][4] + \
						P_DBoard_Port_Original_ENCODE[board_id][port_id][5] + \
						P_DBoard_Port_Original_ENCODE[board_id][port_id][6];
						P_DBoard_Port_ENCODE_Update[board_id][port_id] = 1;
					break;
				}
			}
			break;
		default:break;
	}
}

 void DrvAsynConcentrator::InternalThreadEntry()
{
	size_t board_can_id;
	size_t size{ 0 };
	size_t concentrator_ticks{ 0 };
	while (1)
	{
		auto val = readOctet();
		if(val> 0)
			ParseCanFrameData();
		//Send Fault 
		for (auto it : P_DBoard_Port_Fault) {
			auto board_id = it.first;
			if (P_DBoard_Port_Fault_Update[board_id]) {
				board_can_id = collect_board_id_map_[board_id + 1 ];
				if (board_can_id == 0)
					continue;
				p_t_frame->can_id = concentrator_can_id;
				p_t_frame->can_dlc = 4;
				p_t_frame->data[0] = CAN_PORT_IN_MALFUNCTION;
				p_t_frame->data[1] = it.second >> 8;
				p_t_frame->data[2] = it.second & 0xff;
				p_t_frame->data[3] = board_id + 1;
				writeOctet();
				P_DBoard_Port_Fault_Update[board_id] = 0;
			}
		}
		//send InitTest
		size = P_DBoard_Port_InitTest.size();
		for (auto it : P_DBoard_Port_InitTest) {
			auto board_id = it.first;
			if (P_DBoard_Port_InitTest_Update[board_id]) {
				board_can_id = collect_board_id_map_[board_id + 1];
				if (board_can_id == 0)
					continue;
				p_t_frame->can_id = concentrator_can_id;
				p_t_frame->can_dlc = 4;
				p_t_frame->data[0] = CAN_PORT_IN_INIT_TEST;
				p_t_frame->data[1] = it.second >> 8;
				p_t_frame->data[2] = it.second & 0xff;
				p_t_frame->data[3] = board_id + 1;
				writeOctet();
				P_DBoard_Port_InitTest_Update[board_id] = 0;
			}
		}
		//send P_DBoard_Port_Test
		size = P_DBoard_Port_Test.size();
		for (auto it : P_DBoard_Port_Test) {
			auto board_id = it.first;
			if (P_DBoard_Port_Test_Update[board_id]) {
				board_can_id = collect_board_id_map_[board_id + 1];
				if (board_can_id == 0)
					continue;
				p_t_frame->can_id = concentrator_can_id;
				p_t_frame->can_dlc = 4;
				p_t_frame->data[0] = CAN_PORT_IN_TEST;
				p_t_frame->data[1] = it.second >> 8;
				p_t_frame->data[2] = it.second & 0xff;
				p_t_frame->data[3] = board_id + 1;
				P_DBoard_Port_Test_Update[board_id] = 0;
				writeOctet();
			}
		}
		//Send Concentrator can Id
		if (concentrator_ticks > CONCENTRATOR_TICKS_TIME) {
			p_t_frame->can_id = concentrator_can_id;
			p_t_frame->can_dlc = 5;
			p_t_frame->data[0] = CAN_CONCENTRATOR_CAN_ID;
			p_t_frame->data[1] = concentrator_can_id >> 24;
			p_t_frame->data[2] = (concentrator_can_id >> 16) & 0xff;
			p_t_frame->data[3] = (concentrator_can_id >> 8) & 0xff;
			p_t_frame->data[4] = concentrator_can_id & 0xff;
			writeOctet();
			concentrator_ticks = 0;
		}
		concentrator_ticks++;
	}
}

int DrvAsynConcentrator::readOctet() {

	fd_set fdRead;
	struct timeval t;
	// calculate timeout values
	t.tv_sec = 0;
	t.tv_usec = CAN_QUERY_TIME;

	FD_ZERO(&fdRead);
	FD_SET(_socket, &fdRead);

	// wait until timeout or a message is ready to get read
	int err = select(_socket + 1, &fdRead, NULL, NULL, &t);

	if (0 < err) {
		auto nbytes = read(_socket, pframe, sizeof(can_frame));
		if (0 > nbytes) {
			std::cout << "Error receiving message from device" << std::endl;
		}
	}
	return err;
}

void DrvAsynConcentrator::writeOctet() {

	auto nbytes = write(_socket, p_t_frame, sizeof(can_frame));
	if (0 > nbytes) {
		std::cout << "Error sending message to device" << std::endl;
	}
}

int main(int argc, char** argv) {
	ezcaAutoErrorMessageOff();
	DrvAsynConcentrator*  m_drv= new DrvAsynConcentrator();
	int *rcs, nrcs;
	short port_value = 0;

//	ezcaStartGroup();
//	ezcaGet("Cluster01:DBoard01:Port01:Fault", ezcaShort, 1, &port_value);
//	ezcaEndGroupWithReport(&rcs, &nrcs);
//	std::cout << "port_fault_value:" << port_value << std::endl;
//	ezcaFree((void *)rcs);

	size_t value{ 0 };
	char board_id_c_[8];
	char port_id_c_[8];
	std::string board_id_string;
	std::string port_id_string;

	while (1)
	{
		// store  board_num pv
		size_t board_total_num_r{ 0 };
		for (auto it : m_drv->collect_board_id_map_) {
			if (it.second != 0) {
				board_total_num_r++;
			}
		}

		if (board_total_num_r != m_drv->board_total_num) {
			m_drv->board_total_num = board_total_num_r;
			std::string dboard_num = "Cluster" + m_drv->cluster_id_string + ":DBoardNum";
			ezcaStartGroup();
			ezcaPut((char *)dboard_num.c_str(), ezcaShort, 1, &m_drv->board_total_num);
			ezcaEndGroupWithReport(&rcs, &nrcs);
            ezcaFree((void *)rcs);
		}

		for (size_t i = 0; i < m_drv->board_total_num; i++) {
			short port_fault_value{ 0 };
			short port_init_test{ 0 };
			short port_test{ 0 };
			sprintf(board_id_c_, "%02d", i + 1);
			board_id_string = board_id_c_;
			for (size_t j = 0; j < PORT_MAX_NUM; j++) {
				//char service_buf[64];
				sprintf(port_id_c_, "%02d", j + 1);
				port_id_string = port_id_c_;
				std::string dboard_port_fault = "Cluster" + m_drv->cluster_id_string + ":DBoard" + board_id_string + ":Port" + port_id_string + ":Fault";
				std::string dboard_port_init_test = "Cluster" + m_drv->cluster_id_string + ":DBoard" + board_id_string + ":Port" + port_id_string + ":InitTest";
				std::string dboard_port_test = "Cluster" + m_drv->cluster_id_string + ":DBoard" + board_id_string + ":Port" + port_id_string + ":Test";
				ezcaStartGroup();
				ezcaGet((char *)dboard_port_fault.c_str(), ezcaShort, 1, &port_value);
				ezcaEndGroupWithReport(&rcs, &nrcs);
				ezcaFree((void *)rcs);
				port_fault_value |= port_value << j;

				ezcaStartGroup();
				ezcaGet((char *)dboard_port_init_test.c_str(), ezcaShort, 1, &port_value);
				ezcaEndGroupWithReport(&rcs, &nrcs);
				ezcaFree((void *)rcs);
				port_init_test |= port_value << j;

				ezcaStartGroup();
				ezcaGet((char *)dboard_port_test.c_str(), ezcaShort, 1, &port_value);
				ezcaEndGroupWithReport(&rcs, &nrcs);
				ezcaFree((void *)rcs);
				port_test |= port_value << j;
			}
			//std::cout << "port_fault_value:" << port_fault_value << std::endl;
			//std::cout << "P_DBoard_Port_Fault[i]:" << P_DBoard_Port_Fault[i] << std::endl;
			if (m_drv->P_DBoard_Port_Fault[i] != port_fault_value) {
				m_drv->P_DBoard_Port_Fault_Update[i] = 1;
				m_drv->P_DBoard_Port_Fault[i] = port_fault_value;
			}
			if (m_drv->P_DBoard_Port_InitTest[i] != port_init_test) {
				m_drv->P_DBoard_Port_InitTest_Update[i] = 1;
				m_drv->P_DBoard_Port_InitTest[i] = port_init_test;
			}
			if (m_drv->P_DBoard_Port_Test[i] != port_test) {
				m_drv->P_DBoard_Port_Test_Update[i] = 1;
				m_drv->P_DBoard_Port_Test[i] = port_test;
			}
		}
#if 0
		// store board_num port NonEmpty record
		for (auto it : m_drv->P_DBoard_Port_NonEmpty) {
			auto board_id = it.first;
			auto status = it.second;
			auto status_bk = m_drv->P_DBoard_Port_NonEmpty_bk[board_id];
			if (m_drv->P_DBoard_Port_NonEmpty[board_id] != m_drv->P_DBoard_Port_NonEmpty_bk[board_id])
				m_drv->P_DBoard_Port_NonEmpty_bk[board_id] = m_drv->P_DBoard_Port_NonEmpty[board_id];
			else
				continue;
			auto status_xor = status^status_bk;
			sprintf(board_id_c_, "%02d", board_id);
			board_id_string = board_id_c_;
			for (size_t j = 0; j < PORT_MAX_NUM; j++) {
				auto bit_value = (status_xor >> j) & 0x01;
				if (bit_value) {
					sprintf(port_id_c_, "%02d", j + 1);
					port_id_string = port_id_c_;
					std::string dboard_num_port_nonempty = "Cluster" + m_drv->cluster_id_string + ":DBoard" + board_id_string + ":Port" \
						+ port_id_string + ":NonEmpty";
					port_value = (status >> j) & 0x01;
					ezcaStartGroup();
					ezcaPut((char *)dboard_num_port_nonempty.c_str(), ezcaShort, 1, &port_value);
					ezcaEndGroupWithReport(&rcs, &nrcs);
				}
			}
		}
#endif 
		// store board_num port NonEmpty record
		for(size_t i = 0; i < m_drv->board_total_num; i++) { 
			auto board_id = i + 1;
			auto status = m_drv->P_DBoard_Port_NonEmpty[i + 1];
			auto status_bk = m_drv->P_DBoard_Port_NonEmpty_bk[board_id];
			if (m_drv->P_DBoard_Port_NonEmpty[board_id] != m_drv->P_DBoard_Port_NonEmpty_bk[board_id])
				m_drv->P_DBoard_Port_NonEmpty_bk[board_id] = m_drv->P_DBoard_Port_NonEmpty[board_id];
			else
				continue;
			auto status_xor = status^status_bk;
			sprintf(board_id_c_, "%02d", board_id);
			board_id_string = board_id_c_;
			for (size_t j = 0; j < PORT_MAX_NUM; j++) {
				auto bit_value = (status_xor >> j) & 0x01;
				if (bit_value) {
					sprintf(port_id_c_, "%02d", j + 1);
					port_id_string = port_id_c_;
					std::string dboard_num_port_nonempty = "Cluster" + m_drv->cluster_id_string + ":DBoard" + board_id_string + ":Port" \
						+ port_id_string + ":NonEmpty";
					port_value = (status >> j) & 0x01;
					ezcaStartGroup();
					ezcaPut((char *)dboard_num_port_nonempty.c_str(), ezcaShort, 1, &port_value);
					ezcaEndGroupWithReport(&rcs, &nrcs);
					ezcaFree((void *)rcs);
				}
			}
		}
#if 0
		//store board_num port Led record
		for (auto it : m_drv->P_DBoard_Port_LedOn) {
			auto board_id = it.first;
			auto status = it.second;
			auto status_bk = m_drv->P_DBoard_Port_LedOn_bk[board_id];
			if (m_drv->P_DBoard_Port_LedOn[board_id] != m_drv->P_DBoard_Port_LedOn_bk[board_id])
				m_drv->P_DBoard_Port_LedOn_bk[board_id] = m_drv->P_DBoard_Port_LedOn[board_id];
			else
				continue;
			auto status_xor = status^status_bk;
			sprintf(board_id_c_, "%02d", board_id);
			board_id_string = board_id_c_;
			for (size_t j = 0; j < PORT_MAX_NUM; j++) {
				auto bit_value = (status_xor >> j) & 0x01;
				if (bit_value) {
					sprintf(port_id_c_, "%02d", j + 1);
					port_id_string = port_id_c_;
					std::string dboard_num_port_ledon = "Cluster" + m_drv->cluster_id_string + ":DBoard" + board_id_string + ":Port" \
						+ port_id_string + ":LEDOn";
					port_value = (status >> j) & 0x01;
					ezcaStartGroup();
					ezcaPut((char *)dboard_num_port_ledon.c_str(), ezcaShort, 1, &port_value);
					ezcaEndGroupWithReport(&rcs, &nrcs);
				}
			}
		}
#endif 
		//store board_num port Led record
		for (size_t i = 0; i < m_drv->board_total_num; i++) {
			auto board_id = i + 1;
			auto status = m_drv->P_DBoard_Port_LedOn[i + 1];
			auto status_bk = m_drv->P_DBoard_Port_LedOn_bk[board_id];
			if (m_drv->P_DBoard_Port_LedOn[board_id] != m_drv->P_DBoard_Port_LedOn_bk[board_id])
				m_drv->P_DBoard_Port_LedOn_bk[board_id] = m_drv->P_DBoard_Port_LedOn[board_id];
			else
				continue;
			auto status_xor = status^status_bk;
			sprintf(board_id_c_, "%02d", board_id);
			board_id_string = board_id_c_;
			for (size_t j = 0; j < PORT_MAX_NUM; j++) {
				auto bit_value = (status_xor >> j) & 0x01;
				if (bit_value) {
					sprintf(port_id_c_, "%02d", j + 1);
					port_id_string = port_id_c_;
					std::string dboard_num_port_ledon = "Cluster" + m_drv->cluster_id_string + ":DBoard" + board_id_string + ":Port" \
						+ port_id_string + ":LEDOn";
					port_value = (status >> j) & 0x01;
					ezcaStartGroup();
					ezcaPut((char *)dboard_num_port_ledon.c_str(), ezcaShort, 1, &port_value);
					ezcaEndGroupWithReport(&rcs, &nrcs);
					ezcaFree((void *)rcs);
				}
			}
		}
#if 0
		//store board_num port encode record
		for (auto it : m_drv->P_DBoard_Port_ENCODE) {
			auto board_id = it.first;
			auto port_string = it.second;
			for (auto item : port_string) {
				auto port_id = item.first;
				auto string = item.second;
				if (m_drv->P_DBoard_Port_ENCODE_Update[board_id][port_id]) {
					sprintf(board_id_c_, "%02d", board_id);
					board_id_string = board_id_c_;
					sprintf(port_id_c_, "%02d", port_id + 1);
					port_id_string = port_id_c_;
					std::string dboard_num_port_encode = "Cluster" + m_drv->cluster_id_string + ":DBoard" + board_id_string + ":Port" \
						+ port_id_string + ":ID";
					std::string cmd = "caput " + dboard_num_port_encode + "  " + m_drv->P_DBoard_Port_ENCODE[board_id][port_id];
					system(cmd.c_str());
					m_drv->P_DBoard_Port_ENCODE_Update[board_id][port_id] = 0;
				}
			}
		}
#endif
		for (size_t i = 0; i < m_drv->board_total_num; i++) {
			auto board_id = i + 1;
			auto port_string = m_drv->P_DBoard_Port_ENCODE[i + 1];
			for (auto item : port_string) {
				auto port_id = item.first;
				auto string = item.second;
				if (m_drv->P_DBoard_Port_ENCODE_Update[board_id][port_id]) {
					sprintf(board_id_c_, "%02d", board_id);
					board_id_string = board_id_c_;
					sprintf(port_id_c_, "%02d", port_id + 1);
					port_id_string = port_id_c_;
					std::string dboard_num_port_encode = "Cluster" + m_drv->cluster_id_string + ":DBoard" + board_id_string + ":Port" \
						+ port_id_string + ":ID";
					std::string cmd = "caput " + dboard_num_port_encode + "  " + m_drv->P_DBoard_Port_ENCODE[board_id][port_id];
					system(cmd.c_str());
					m_drv->P_DBoard_Port_ENCODE_Update[board_id][port_id] = 0;
				}
			}
		}
		usleep(1);
	}
	return 1;
}