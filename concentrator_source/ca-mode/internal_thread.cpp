#include "internal_thread.h"

void* InternalThread::run0(void* opt)
{
	InternalThread* p = (InternalThread*)opt;
     p->InternalThreadEntry();
     return p;
 }

bool InternalThread::StartInternalThread()
{
	size_t status = 0;
    pthread_create(&_tid, NULL, run0, this);
 }

void InternalThread::join()
{
	if(_tid > 0)
		 pthread_join(_tid, NULL);
}




















