#pragma once

#include "pthread.h"

class InternalThread {
public:
	explicit InternalThread() {};
	bool StartInternalThread();
	virtual void InternalThreadEntry() {};
protected:
	pthread_t _tid;
	pthread_t _tid_normal;
	static void* run0(void* opt);
	void join();
};






