#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <linux/input.h>
#include <unistd.h>
#include <stdlib.h>
#include <iostream>

#define Optical_Reset_Cmd 		0xFFFF00
#define Optical_Control_Cmd 	0xFFFF01

int main(int argc, char **argv)
{
	char *filename;
    int fd = open("/dev/opticalswitch",O_RDWR);
    unsigned int str[1];
    int cmd = 0;
    int channel = 9;
    int status = 0;
	if(fd < 0){
		std::cout << "Open opticalswitch driver failed" << std::endl;
		exit(-1);
	}

	while(1)
	{
		status = read(fd,str,sizeof(int));
		std::cout << "status:" << status << std::endl;
		if(status == 0)
		{
			ioctl(fd, Optical_Control_Cmd, channel);
			break;
		}
		else
		{
			usleep(10000);
		}
	}
	return 0;
}


//arm-poky-linux-gnueabi-gcc  -march=armv7-a -mfpu=neon  -mfloat-abi=hard -mcpu=cortex-a9 --sysroot=/opt/fsl-imx-fb/4.1.15-2.1.0/sysroots/cortexa9hf-neon-poky-linux-gnueabi -o main main.cpp
//${CROSS_COMPILE}gcc