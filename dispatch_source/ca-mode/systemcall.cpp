#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <linux/input.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <map>
#include <vector>
#include <cadef.h>
#include <ezca.h>

#define Optical_Reset_Cmd 	0xFFFF00
#define Optical_Control_Cmd 	0xFFFF01

std::map<std::string, std::string> extend_string_pv_;
int main(void)
{
    char buf[1024]={0};
    int fd[2];
    int backfd;
	int status = 0;
	int device_fd = open("/dev/opticalswitch", O_RDWR);
	unsigned int str[1];
	if (device_fd < 0) {
		std::cout << "Open opticalswitch driver failed" << std::endl;
		exit(-1);
	}
	usleep(5000000);
    pipe(fd);
    backfd=dup(STDOUT_FILENO);//备份标准输出，用于恢复

	char *path_file = getenv("EXTERNFILE");
	std::ifstream input(path_file, std::ios_base::in);
	std::string line;
	if (!input.is_open())
		fprintf(stderr, "CONCENTRATORFILE not set\n\r");

	while (std::getline(input, line)) {
		// handle comments in config file
		size_t comment;
		if ((comment = line.find('#')) != std::string::npos)
			line.erase(comment); // erase everthing after first '#' from string

		if (0 == line.length()) continue; // string is empty, no need to parse it
		std::string type;
		std::string value;
		std::istringstream parse(line);
		parse >> type;
		parse >> value;
		if (type.find("ID") != std::string::npos || \
			type.find("BoardId") != std::string::npos || \
			type.find("StationName") != std::string::npos || \
			type.find("StationPort") != std::string::npos)
			extend_string_pv_[type] = value;
	}

	std::string board_id_string = extend_string_pv_["BoardId:"];
	board_id_string = board_id_string.size()>=2 ? board_id_string : "0"+ board_id_string;

	std::string cmd = "caput  ExtDBoard" + board_id_string + ":ID " + extend_string_pv_["ID:"];
	system(cmd.c_str());

	cmd = "caput  ExtDBoard"+ board_id_string +":StationName " + extend_string_pv_["StationName:"];
	system(cmd.c_str());

	cmd = "caput  ExtDBoard"+ board_id_string +":StationPort " + extend_string_pv_["StationPort:"];
	system(cmd.c_str());

	std::string switch_string = "ExtDBoard" + board_id_string + ":Switch";
	ezcaAutoErrorMessageOff();
	int *rcs, nrcs;
	short channel{ 0 };
	short channel_bk{ 0 };
	while (1) {
		ezcaStartGroup();
		ezcaGet((char *)switch_string.c_str(), ezcaShort, 1, &channel);
		ezcaEndGroupWithReport(&rcs, &nrcs);
		ezcaFree((void *)rcs);
		if(channel_bk != channel){
			status = read(device_fd, str, sizeof(int));
			if (status == 0)
			{
				ioctl(device_fd, Optical_Control_Cmd, channel);
			}
		   channel_bk = channel;
		}	
		usleep(100000);
	}
    return 0;
}













