#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cerrno>
#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/select.h>
#include <sys/stat.h>
#include <unistd.h>

// EPICS includes
#include <epicsEvent.h>
#include <epicsExport.h>
#include <epicsMutex.h>
#include <epicsString.h>
#include <epicsThread.h>
#include <epicsTime.h>
#include <epicsTimer.h>
#include <epicsTypes.h>
#include <iocsh.h>
#include <iostream>
// local includes
#include "dispatch_driver.h"

#define Optical_Reset_Cmd     0xFFFF00
#define Optical_Control_Cmd   0xFFFF01

static const char *driverName="drvDispatch";

void arrayGenTaskC(void *drvPvt)
{
    drvDispatch *pPvt = (drvDispatch *)drvPvt;
    pPvt->arrayGenTask();
}

drvDispatch::drvDispatch( const char *portName, const char *ttyName )
   : asynPortDriver(portName, 
                    1, /* maxAddr */ 
                    asynInt32Mask | asynFloat64Mask | asynInt32ArrayMask | asynDrvUserMask, /* Interface mask */
                    asynInt32Mask | asynFloat64Mask | asynInt32ArrayMask,                   /* Interrupt mask */
                    0, /* asynFlags.  This driver does not block and it is not multi-device, so flag is 0 */
                    1, /* Autoconnect */
                    0, /* Default priority */
                    0) /* Default stack size*/  
{
  asynStatus status;
  const char *functionName = "drvDispatch";  

  device_fd = open("/dev/opticalswitch", O_RDWR);
  if (device_fd < 0) {
    std::cout << "Open opticalswitch driver failed" << std::endl;
    exit(-1);
  }

  /* Make sure maxArrayLength is positive */
  //if (maxArrayLength < 1) maxArrayLength = 10;
  
  /* Allocate the waveform array */
 // pData_ = (epicsInt32 *)calloc(maxArrayLength, sizeof(epicsInt32));
  eventId_ = epicsEventCreate(epicsEventEmpty);
  createParam(P_SwitchString,  asynParamInt32, &P_Switch);

  /* Create the thread that does the array callbacks in the background */
  status = (asynStatus)(epicsThreadCreate("drvDispatch",
                        epicsThreadPriorityMedium,
                        epicsThreadGetStackSize(epicsThreadStackMedium),
                        (EPICSTHREADFUNC)::arrayGenTaskC,
                        this) == NULL);
  if (status) {
      printf("%s::%s: epicsThreadCreate failure\n", driverName, functionName);
      return;
  }
}

drvDispatch::~drvDispatch(){
  if(device_fd >= 0)
    close(device_fd);
}


void drvDispatch::arrayGenTask(void)
{
    epicsInt32 switch_c; 
    while (1) {
        //getIntegerParam(P_Switch, &switch_c);
        //if (switch_c) epicsEventWaitWithTimeout(eventId_, 1);
        //else         (void)epicsEventWait(eventId_);
        //getIntegerParam(P_RunStop, &switch_c);
        epicsThreadSleep(1);
    }
}

void drvDispatch::ExecuteDriver(unsigned int value){
  unsigned int str[1];
  printf("driver send cmd value:%d\n\r",value);
  unsigned int status = read(device_fd, str, sizeof(int));
  if (status == 0)
    ioctl(device_fd, Optical_Control_Cmd, value);
}

asynStatus drvDispatch::writeInt32(asynUser *pasynUser, epicsInt32 value)
{
    int function = pasynUser->reason;
    asynStatus status = asynSuccess;
    const char *paramName;
    const char* functionName = "writeInt32";

    /* Set the parameter in the parameter library. */
    status = (asynStatus) setIntegerParam(function, value);
    
    /* Fetch the parameter string name for possible use in debugging */
    getParamName(function, &paramName);
    if (function == P_Switch) {
        ExecuteDriver(value);
        if (value) epicsEventSignal(eventId_);
    } 
    else {
        /* All other parameters just get set in parameter list, no need to
         * act on them here */
    }
    
    /* Do callbacks so higher layers see any changes */
    status = (asynStatus) callParamCallbacks();
    
    if (status) 
        epicsSnprintf(pasynUser->errorMessage, pasynUser->errorMessageSize, 
                  "%s:%s: status=%d, function=%d, name=%s, value=%d", 
                  driverName, functionName, status, function, paramName, value);
    else        
        asynPrint(pasynUser, ASYN_TRACEIO_DRIVER, 
              "%s:%s: function=%d, name=%s, value=%d\n", 
              driverName, functionName, function, paramName, value);
    return status;
}