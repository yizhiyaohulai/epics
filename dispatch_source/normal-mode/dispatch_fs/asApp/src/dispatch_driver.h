#ifndef __ASYN_DISPATCH_DRIVER__
#define __ASYN_DISPATCH_DRIVER__

#include "asynPortDriver.h"

#define P_SwitchString            "RUN_STOP"            /* asynInt32,    r/w */

class drvDispatch : public asynPortDriver {
public:
  	drvDispatch( const char *portName, const char *ttyName );
  	~drvDispatch();
  	virtual asynStatus writeInt32(asynUser *pasynUser, epicsInt32 value);
  	void arrayGenTask(void);
protected:
  	int P_Switch;
private:
	epicsEventId eventId_;
    epicsInt32 *pData_;
    void ExecuteDriver(unsigned int value);
    int device_fd;
};

#endif
