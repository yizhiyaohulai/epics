#include <cstdio>
#include <cstdlib>
#include <cstring>

// EPICS includes
#include <epicsEvent.h>
#include <epicsExport.h>
#include <epicsMutex.h>
#include <epicsString.h>
#include <epicsThread.h>
#include <epicsTime.h>
#include <epicsTimer.h>
#include <epicsTypes.h>
#include <iocsh.h>

// local includes
#include "dispatch_driver.h"

extern "C" {
  int drvdispatchConfigure( const char *portName, const char *ttyName ) {
    new drvDispatch( portName, ttyName );
    return( asynSuccess );
  }
  static const iocshArg initArg0 = { "portName", iocshArgString };
  static const iocshArg initArg1 = { "ttyName",  iocshArgString };
  static const iocshArg * const initArgs[] = { &initArg0, &initArg1 };
  static const iocshFuncDef initFuncDef = { "drvdispatchConfigure", 2, initArgs };
  static void initCallFunc( const iocshArgBuf *args ) {
    drvdispatchConfigure( args[0].sval, args[1].sval );
  }
  //----------------------------------------------------------------------------
  //! @brief   Register functions to EPICS
  //----------------------------------------------------------------------------
  void dispatchRegister( void ) {
    iocshRegister( &initFuncDef, initCallFunc );
  }
  epicsExportRegistrar( dispatchRegister );
}
